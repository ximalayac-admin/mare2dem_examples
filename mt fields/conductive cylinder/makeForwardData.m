 
sFile = 'cylinder.emdata';   % output file name
topo  = [];

clear st;

st.stMT.frequencies  = [ 1.0 10];

% grid of receivers:
y     = -3000:100:3000;
z     = [0:100:3000] + 0.1; % +0.1 to nudge the top row into the ground
[Y,Z] = meshgrid(y,z);

st.stMT.rx_y         = Y(:);
st.stMT.rx_z         = Z(:);
st.stMT.lMTFields    = true;  

% make data:
m2d_makeDataFile(sFile,topo,'forward',st)

% plot .resistivity file with MT receivers on top to confirm receivers are
% located at the correct positions and depth:
plotMARE2DEM('newest'); % 'newest' plots the most recent .resistivity file
 
 