 
sFile = 'amphibious.emdata'; 
topo = load('../topo_mb_simplified.txt');
  
st.stMT.frequencies  = logspace(-4,0,21);
st.stMT.rx_y         = [-10:1:35]'*1e3;
st.stMT.rx_type      = 'amphibious';
st.stMT.lTE          = true;
st.stMT.lTM          = true;
 
m2d_makeDataFile(sFile,topo,'forward',st)
 
plotMARE2DEM('newest')