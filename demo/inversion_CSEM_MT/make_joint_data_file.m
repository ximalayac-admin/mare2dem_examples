
% This script shows how to take an MT-only data file and a 
% CSEM-only data file and merge them together to create a joint MT-CSEM
% data file using the function m2d_mergeDataFiles.m.
%

sMTDataFile  = '../inversion_MT/demo_mt_synth.emdata';
sCSEMDataFile = '../inversion_CSEM/demo_csem_synth.emdata';

sOutputFileName = 'demo_csem_mt_synth.emdata';

% Output the joint MT-CSEM data file:
m2d_mergeDataFiles(sMTDataFile,sCSEMDataFile,sOutputFileName);
