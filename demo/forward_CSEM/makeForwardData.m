
clear st;

sFile = 'demo_csem.emdata'; 
topo  = load('../Topo.txt');

stype = 'forward';

st.stCSEM.frequencies  = [0.75];
st.stCSEM.rx_y         = 0:2000:24000;
st.stCSEM.rx_type      = 'marine';
 
st.stCSEM.tx_y        = -1000:2000:25000;
st.stCSEM.tx_z_offset = -50;
st.stCSEM.tx_vector = 'parallel';

 
st.stCSEM.min_range = 500;
st.stCSEM.max_range = 15000;
st.stCSEM.lEy = true;
 

m2d_makeDataFile(sFile,topo,stype,st)
 
 
plotMARE2DEM('newest') % show Tx and Rx on model