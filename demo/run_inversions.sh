#!/usr/bin/env -S -i zsh  # ensure clean environment.

m2d="mpirun -n 9 -oversubscribe MARE2DEM demo.0.resistivity"
mtlb="/Applications/MATLAB_R2020b.app/bin/matlab"

# Run forward_MT, generate synthetic noisy data and distribute to inversion folders: 
cd ./forward_MT
eval "$m2d"
eval "$mtlb -batch 'makeSynthInversionData' "
cp  demo_mt_synth.emdata ../inversion_MT
cp  demo_mt_synth.emdata ../inversion_MT_static
cp  demo_mt_synth.emdata ../inversion_MT_cut


# Run forward_CSEM, generate synthetic noisy data and distribute to inversion folders: 
cd ../forward_CSEM
eval "$m2d"
eval "$mtlb -batch 'makeSynthInversionData' "
cp  demo_csem_synth.emdata ../inversion_CSEM
cd ../inversion_CSEM_MT
eval "$mtlb -batch 'make_joint_data_file' "
 
# Now run all inversions:
cd ../inversion_MT
eval "$m2d"
cd ../inversion_MT_static
eval "$mtlb -batch 'makeSyntheticStaticShift' "
eval "$m2d"
cd ../inversion_MT_cut
eval "$m2d"
cd ../inversion_CSEM
eval "$m2d"
cd ../inversion_CSEM_MT
eval "$m2d"